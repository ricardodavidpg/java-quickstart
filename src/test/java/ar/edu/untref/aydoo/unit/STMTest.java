package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.Spec;
import ar.edu.untref.aydoo.stm.Card;
import ar.edu.untref.aydoo.stm.STM;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.Bidi;
import java.util.Map;

/**
 * Created by ideasoft on 16/10/2018.
 */
public class STMTest {

    @Test
    public void payTest()
    {
        //Arange
        STM stm = new STM();
        Card card = new Card(1);
        stm.addCard(card);

        //Act
        stm.chargeCard(1,BigDecimal.valueOf(29));
        stm.pay(1,BigDecimal.valueOf(29));

        //Asert
        BigDecimal balance = stm.getBlance(1);
        Assert.assertTrue(balance.equals(BigDecimal.ZERO));
    }

    @Test
    public void payWithDiscount(){
        //Arange
        STM stm = new STM();
        Card card = new Card(1);
        card.setWithDiscount(true);

        stm.addCard(card);

        //Act
        stm.chargeCard(1,BigDecimal.valueOf(29));
        stm.pay(1,BigDecimal.valueOf(29));

        //Asert
        BigDecimal balance = stm.getBlance(1);
        Assert.assertTrue(balance.equals(BigDecimal.TEN));
    }


}
