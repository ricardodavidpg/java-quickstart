package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.Foo;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ideasoft on 16/10/2018.
 */
public class StringTest {

    final static String TEST_STRING = "workshoptdd";

    @Test
    public void lenghtTest()
    {
       Assert.assertTrue(TEST_STRING.length()==11);
    }

    @Test
    public void nullTest()
    {
        String nullString = null;
        Assert.assertNull(nullString);
    }

    @Test
    public void upperTest()
    {
        Assert.assertEquals(TEST_STRING.toUpperCase(),"WORKSHOPTDD");
    }

    @Test
    public void lowerTest()
    {
        String upperString = "WORKSHOPTDD";
        Assert.assertEquals(upperString.toLowerCase(),TEST_STRING);
    }



}
