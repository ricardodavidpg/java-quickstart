package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.Spec;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ideasoft on 16/10/2018.
 */
public class SpecsTest {

    @Test
    public void emptyTest()
    {
        //Arange
        Spec spec = new Spec();

        //Act
        String example = "";
        Map<String, Integer> result = spec.calulate(example);

        //Asert
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void nullTest()
    {
        //Arange
        Spec spec = new Spec();

        //Act
        String example = null;
        Map<String, Integer> result = spec.calulate(example);

        //Asert
        Assert.assertNull(result);
    }

    @Test
    public void mamaTest()
    {
        //Arange
        Spec spec = new Spec();

        //Act
        String example = "mama";
        Map<String, Integer> result = spec.calulate(example);

        //Asert
        Map<String, Integer> expected = new HashMap<>();
        expected.put("m", 2);
        expected.put("a", 2);

        Assert.assertEquals(result,expected);
    }
}
