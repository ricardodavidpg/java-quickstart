package ar.edu.untref.aydoo.stm;

import java.math.BigDecimal;

/**
 * Created by ideasoft on 16/10/2018.
 */
public class Card {
    private long id;
    private BigDecimal balance;
    private boolean withDiscount;

    public Card(long id){
        this.id =id;
        this.balance = BigDecimal.ZERO;
        this.withDiscount = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setWithDiscount(boolean withDiscount) {
        this.withDiscount = withDiscount;
    }

    public boolean isWithDiscount() {
        return withDiscount;
    }
}
