package ar.edu.untref.aydoo.stm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ideasoft on 16/10/2018.
 */
public class STM {

    private Map<Long, Card> cardList;

    public STM() {
        this.cardList = new HashMap<>();
    }

    public void addCard(Card card) {
        cardList.put(card.getId(), card);
    }

    public void chargeCard(long id, BigDecimal balance) {
        Card card = cardList.get(id);
        card.setBalance(card.getBalance().add(balance));
    }

    public void pay(long id, BigDecimal balance) {
        Card card = cardList.get(id);
        if(card.isWithDiscount()){
           balance = balance.subtract(BigDecimal.TEN);
        }
        card.setBalance(card.getBalance().subtract(balance));
    }

    public BigDecimal getBlance(long id) {
        Card card = cardList.get(id);
        return card.getBalance();
    }
}
