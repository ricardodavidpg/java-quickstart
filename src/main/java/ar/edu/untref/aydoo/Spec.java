package ar.edu.untref.aydoo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ideasoft on 16/10/2018.
 */
public class Spec {
    public Map<String,Integer> calulate(String example) {

        if(example==null){
            return null;
        }

        Map<String, Integer> result = new HashMap<>();
        char[] chars = example.toCharArray();

        for (char c: chars) {
            Integer ocurrences = result.get(Character.toString(c));
            if(ocurrences==null){
                ocurrences=0;
            }

            result.put(Character.toString(c),ocurrences + 1);
        }

        return result;
    }
}
